from unittest.mock import Mock

# Let's mock 'json' library
json = Mock()

def check_json(json_str):
    json_object = json.loads(json_str)
    return json_object

def test_json_loads_called():
    json_str = '{"predicted_index": "41.19987","node_trend": "down"}'
    assert isinstance(json, Mock)
    check_json(json_str)
    #json.loads.assert_not_called()
    json.loads.assert_called_once_with(json_str)